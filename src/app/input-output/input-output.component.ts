import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-input-output',
  templateUrl: './input-output.component.html',
  styleUrls: ['./input-output.component.sass']
})

export class InputOutputComponent implements OnInit {

  @Input() item= 0;
  @Output() itemChange = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

  dec() { this.resize(-1); }
  inc() { this.resize(+1); }

  resize(delta: number) {
    this.item = Math.min(40, Math.max(8, +this.item + delta));
    this.itemChange.emit(this.item);
  }

}
