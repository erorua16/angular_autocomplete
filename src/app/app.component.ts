import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {

  items = ['item1', 'item2', 'item3', 'item4'];
  fontSizePx = 16;

  addItem(newItem: string) {
    this.items.push(newItem);
  }


  title = 'angular_autocomplete';
  currentItem = 'Television';
}
