import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.sass']
})
export class AutocompleteComponent implements OnInit {

  number:number = 0

  @Input() item = '';

  constructor() { }

  ngOnInit(): void {
  }

}
